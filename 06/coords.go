package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	X, Y int
}

type Points []*Point
type Space struct {
	W, H    int
	Closest []*Point
	Points  Points
}

func ReadPoints(r io.Reader) Points {
	var ret Points
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		parts := strings.Split(sc.Text(), ", ")
		x, err := strconv.Atoi(parts[0])
		if err != nil {
			log.Fatal(err)
		}
		y, err := strconv.Atoi(parts[1])
		if err != nil {
			log.Fatal(err)
		}
		ret = append(ret, &Point{x, y})
	}
	return ret
}

func (p Point) DistanceTo(x, y int) int {
	dX := x - p.X
	dY := y - p.Y
	if dX < 0 {
		dX *= -1
	}
	if dY < 0 {
		dY *= -1
	}
	return dX + dY
}

func (p Points) Space() *Space {
	// our coordinate space goes from zero to the maximum coordinate value in
	// each direction, flattened into a single backing array.
	var maxX, maxY int
	for _, point := range p {
		if point.X > maxX {
			maxX = point.X
		}
		if point.Y > maxY {
			maxY = point.Y
		}
	}
	sp := &Space{
		W:      maxX + 1,
		H:      maxY + 1,
		Points: p,
	}
	sp.Closest = make([]*Point, sp.W*sp.H)

	// Now we'll populate Closest in a brute-force sort of way by visiting
	// every point for every coordinate to compute the Manhattan distance.
	for y := 0; y < sp.H; y++ {
		for x := 0; x < sp.W; x++ {
			dists := make(map[*Point]int, len(p))
			for _, point := range p {
				dists[point] = point.DistanceTo(x, y)
			}
			var closest *Point
			dist := sp.W + sp.H + 1 // This must be bigger than any "real" distance
			for point, thisDist := range dists {
				if thisDist < dist {
					closest = point
					dist = thisDist
				}
			}
			// If any others are the same distance then there is no closest
			for point, thisDist := range dists {
				if point != closest && thisDist == dist {
					closest = nil
					break
				}
			}
			sp.Closest[sp.offset(x, y)] = closest // might be nil, if two are equally-close
		}
	}

	return sp
}

func (s *Space) offset(x, y int) int {
	return (y * s.W) + x
}

func (s *Space) coords(offset int) (int, int) {
	x := offset % s.W
	y := offset / s.W
	return x, y
}

func (s *Space) isInfinity(offset int) bool {
	x, y := s.coords(offset)
	return x <= 0 || y <= 0 || x >= (s.W-1) || y >= (s.H-1)
}

// Area returns the area covered by the region where the closest point is the
// given point, or -1 if that area is infinite.
func (s *Space) Area(p *Point) int {
	area := 0
	for i, closestP := range s.Closest {
		if closestP != p {
			continue
		}
		if s.isInfinity(i) {
			return -1
		}
		area++
	}
	return area
}

func (s *Space) SafeRegionSize(limit int) int {
	// Points outside of our space bounding box can be part of the safe
	// region, so for our purposes here we're going to triple the size
	// of the bounding box in each direction. That means it is not safe to
	// index into s.Closest in this loop, but that's fine because we don't
	// need it.
	safeRegionArea := 0
	for y := -s.H; y < (s.H * 2); y++ {
		for x := -s.W; x < (s.W * 2); x++ {
			sumDists := 0
			for _, p := range s.Points {
				sumDists += p.DistanceTo(x, y)
			}
			if sumDists < limit {
				safeRegionArea++
			}
		}
	}
	return safeRegionArea
}

func main() {
	points := ReadPoints(os.Stdin)
	space := points.Space()
	var largestPoint *Point
	largestArea := -1
	for _, p := range points {
		area := space.Area(p)
		if area > largestArea {
			largestArea = area
			largestPoint = p
		}
	}
	log.Printf("Most spacious point is %#v, covering %d square units", largestPoint, largestArea)

	limit := 10000
	if len(points) < 10 {
		// It's the test input, then
		limit = 32
	}
	safeSize := space.SafeRegionSize(limit)
	log.Printf("Safe region has %d square units", safeSize)
}

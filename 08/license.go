package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"strconv"
)

type Node struct {
	Metadata []int
	Children []*Node
}

func (n *Node) MetadataSum() int {
	var ret int
	for _, v := range n.Metadata {
		ret += v
	}
	for _, c := range n.Children {
		ret += c.MetadataSum()
	}
	return ret
}

func (n *Node) Value() int {
	if len(n.Children) == 0 {
		return n.MetadataSum()
	}
	var ret int
	for _, v := range n.Metadata {
		idx := v - 1
		if idx >= len(n.Children) {
			continue
		}
		ret += n.Children[idx].Value()
	}
	return ret
}

func ParseNode(tokens []int) (node *Node, remain []int) {
	if len(tokens) < 2 {
		log.Fatal("invalid token header")
	}
	ret := &Node{}
	nodeCount := tokens[0]
	metaCount := tokens[1]
	tokens = tokens[2:]
	if nodeCount > 0 {
		ret.Children = make([]*Node, 0, nodeCount)
		for i := 0; i < nodeCount; i++ {
			child, remain := ParseNode(tokens)
			ret.Children = append(ret.Children, child)
			tokens = remain
		}
	}
	if metaCount > 0 {
		ret.Metadata = tokens[:metaCount]
		tokens = tokens[metaCount:]
	}
	return ret, tokens
}

func Scan(r io.Reader) []int {
	var ret []int
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	for sc.Scan() {
		l := sc.Text()
		v, err := strconv.Atoi(l)
		if err != nil {
			log.Fatal(err)
		}
		ret = append(ret, v)
	}
	return ret
}

func main() {
	tokens := Scan(os.Stdin)
	rootNode, remain := ParseNode(tokens)
	if len(remain) != 0 {
		log.Fatalf("straggler tokens after root node %#v", remain)
	}
	log.Printf("Sum of all metadata is %d", rootNode.MetadataSum())
	log.Printf("Value of the root node is %d", rootNode.Value())
}

package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

var claimPattern = regexp.MustCompile(`^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$`)

const size = 1000

type Claim struct {
	ID         int
	X, Y, W, H int
}

func ReadClaims(r io.Reader) map[int]Claim {
	sc := bufio.NewScanner(os.Stdin)
	claims := make(map[int]Claim)
	for sc.Scan() {
		l := sc.Text()
		partsStr := claimPattern.FindStringSubmatch(l)
		parts := make([]int, len(partsStr)-1)
		for i, s := range partsStr {
			if i == 0 {
				continue
			}
			var err error
			parts[i-1], err = strconv.Atoi(s)
			if err != nil {
				log.Fatal(err)
			}
		}
		claims[parts[0]] = Claim{
			ID: parts[0],
			X:  parts[1],
			Y:  parts[2],
			W:  parts[3],
			H:  parts[4],
		}
	}
	return claims
}

func PlotClaims(claims map[int]Claim) [][]int {
	ret := make([][]int, size*size)
	for _, claim := range claims {
		for y := 0; y < claim.H; y++ {
			for x := 0; x < claim.W; x++ {
				idx := (y+claim.Y)*size + x + claim.X
				ret[idx] = append(ret[idx], claim.ID)
			}
		}
	}
	return ret
}

func main() {
	claims := ReadClaims(os.Stdin)
	plot := PlotClaims(claims)
	reused := 0
	for _, ids := range plot {
		if len(ids) >= 2 {
			reused++
		}
	}
	fmt.Printf("%d inches reused\n", reused)

Claims:
	for _, claim := range claims {
		for y := 0; y < claim.H; y++ {
			for x := 0; x < claim.W; x++ {
				idx := (y+claim.Y)*size + x + claim.X
				if len(plot[idx]) != 1 {
					continue Claims
				}
			}
		}
		fmt.Printf("%d is uncontested (%d,%d %dx%d)\n", claim.ID, claim.X, claim.Y, claim.W, claim.H)
	}
}

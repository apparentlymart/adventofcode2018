package main

import (
	"bufio"
	"log"
	"os"
)

func Checksum(ids []string) int {
	var twos, threes int
	for _, id := range ids {
		letters := make(map[rune]int)
		for _, letter := range id {
			letters[letter]++
		}
		for _, c := range letters {
			if c == 2 {
				twos++
				break
			}
		}
		for _, c := range letters {
			if c == 3 {
				threes++
				break
			}
		}
	}
	return twos * threes
}

func CommonLetters(ids []string) string {
	common := make([]byte, 0, len(ids[0]))
	for i, id1 := range ids {
		for j, id2 := range ids {
			if i == j {
				continue
			}
			common = common[:0]
			for k := 0; k < len(id1); k++ {
				if id1[k] == id2[k] {
					common = append(common, id1[k])
				}
			}
			if len(common) == len(id1)-1 {
				return string(common)
			}
		}
	}
	return ""
}

func main() {
	sc := bufio.NewScanner(os.Stdin)
	var ids []string
	for sc.Scan() {
		ids = append(ids, sc.Text())
	}
	checksum := Checksum(ids)
	log.Printf("Checksum is %d", checksum)
	common := CommonLetters(ids)
	log.Printf("Common letters are is %s", common)
}

package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
)

type Unit byte
type Polarity byte
type Type byte
type TypeSet map[Type]struct{}

func (u Unit) Polarity() Polarity {
	return Polarity(u & 0x20)
}

func (u Unit) Type() Type {
	return Type(u & 0x5f)
}

func (t Type) String() string {
	return string([]byte{byte(t)})
}

func (s TypeSet) Has(t Type) bool {
	_, has := s[t]
	return has
}

func (s TypeSet) Put(t Type) {
	s[t] = struct{}{}
}

type Polymer []Unit

func NewPolymer(raw []byte) Polymer {
	ret := make(Polymer, len(raw))
	for i, b := range raw {
		ret[i] = Unit(b)
	}
	return ret
}

func (p Polymer) ReactOnce() (Polymer, bool) {
	for i := 0; i < len(p)-1; i++ {
		a, b := p[i], p[i+1]
		if a.Type() != b.Type() {
			continue
		}
		if a.Polarity() == b.Polarity() {
			continue
		}
		// If we get here then we've found a pair of the same type but
		// differing polarity, so we'll remove it by copying the remainder
		// of the polymer over it and then truncating it.
		copy(p[i:], p[i+2:])
		p = p[:len(p)-2]
		return p, true
	}
	return p, false
}

func (p Polymer) DistinctTypes() TypeSet {
	ret := make(TypeSet)
	for _, unit := range p {
		ret.Put(unit.Type())
	}
	return ret
}

func (p Polymer) ReactFully() Polymer {
	for {
		var changed bool
		p, changed = p.ReactOnce()
		if !changed {
			return p
		}
	}
}

func (p Polymer) RemoveType(t Type) Polymer {
	for i := len(p) - 1; i >= 0; i-- {
		if p[i].Type() != t {
			continue
		}
		copy(p[i:], p[i+1:])
		p = p[:len(p)-1]
	}
	return p
}

func (p Polymer) String() string {
	b := make([]byte, len(p))
	for i, u := range p {
		b[i] = byte(u)
	}
	return string(b)
}

func (p Polymer) Len() int {
	return len(p)
}

func (p Polymer) Copy() Polymer {
	ret := make(Polymer, len(p))
	copy(ret, p)
	return ret
}

func main() {
	raw, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	raw = bytes.TrimSpace(raw)
	polymer := NewPolymer(raw)
	reacted := polymer.Copy().ReactFully()
	log.Printf("After fully reacting, polymer has %d entries left", reacted.Len())

	bestLen := len(polymer)
	var bestLenType Type
	for ty := range polymer.DistinctTypes() {
		p := polymer.Copy().RemoveType(ty)
		p = p.ReactFully()
		if p.Len() < bestLen {
			bestLen = p.Len()
			bestLenType = ty
		}
	}
	log.Printf("After removing %s and fully reacting, polymer has %d entries left", bestLenType, bestLen)
}

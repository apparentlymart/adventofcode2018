package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"strconv"
)

func GetFreq(r io.Reader) int {
	sc := bufio.NewScanner(r)
	freq := 0
	for sc.Scan() {
		l := sc.Text()
		v, err := strconv.Atoi(l)
		if err != nil {
			log.Fatal(err)
		}
		freq += v
	}
	return freq
}

func GetFirstDupFreq(r io.Reader) int {
	sc := bufio.NewScanner(r)
	var changes []int
	for sc.Scan() {
		l := sc.Text()
		v, err := strconv.Atoi(l)
		if err != nil {
			log.Fatal(err)
		}
		changes = append(changes, v)
	}

	freq := 0
	freqsSeen := make(map[int]int)
	for {
		for _, v := range changes {
			if freqsSeen[freq] > 0 {
				return freq
			} else {
				freqsSeen[freq]++
			}

			freq += v
		}
	}
}

func main() {
	//freq := GetFreq(os.Stdin)
	//log.Printf("Resulting frequency is %d\n", freq)
	freqTwice := GetFirstDupFreq(os.Stdin)
	log.Printf("First frequency reached twice is %d\n", freqTwice)
}

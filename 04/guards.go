package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type Day struct {
	Date    string
	GuardID int
	Asleep  [60]bool
}

func ParseLog(lines []string) []Day {
	sort.Strings(lines)

	var lastGuardID int
	var day Day
	var days []Day
	for _, line := range lines {
		date := line[6:11]
		if date != day.Date {
			if day.Date != "" {
				days = append(days, day)
			}
			day = Day{
				Date: date,
			}
			log.Printf("start of day %s", day.Date)
		}
		evt := line[19:]
		evtTy := evt[:5]
		switch evtTy {
		case "Guard":
			idStr := line[26:]
			sp := strings.Index(idStr, " ")
			idStr = idStr[:sp]
			id, err := strconv.Atoi(idStr)
			if err != nil {
				log.Fatal(err)
			}
			lastGuardID = id
			log.Printf("guard %d on duty", id)
		case "falls":
			day.GuardID = lastGuardID
			minStr := line[15:17]
			min, err := strconv.Atoi(minStr)
			if err != nil {
				log.Fatal(err)
			}
			day.Asleep[min] = true
			log.Printf("guard %d falls asleep at 00:%02d", day.GuardID, min)
		case "wakes":
			minStr := line[15:17]
			min, err := strconv.Atoi(minStr)
			if err != nil {
				log.Fatal(err)
			}
			for i := min - 1; i >= 0; i-- {
				if day.Asleep[i] {
					break
				}
				day.Asleep[i] = true
			}
			log.Printf("guard %d wakes up at 00:%02d", day.GuardID, min)
		}
	}
	days = append(days, day)
	return days
}

func main() {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}

	days := ParseLog(lines)

	sleepyGuards := make(map[int]int)
	for _, day := range days {
		sleepMins := 0
		for _, asleep := range day.Asleep {
			if asleep {
				sleepMins++
			}
		}
		if day.GuardID == 0 && sleepMins > 0 {
			log.Fatal("got some sleep hours on %d even though there is no GuardID", day.Date)
		}
		if day.GuardID > 0 {
			sleepyGuards[day.GuardID] += sleepMins
			log.Printf("On %s guard %d slept for %d hours", day.Date, day.GuardID, sleepMins)
		}
	}
	var mostSleep, sleepiestGuard int
	for guardID, sleepMins := range sleepyGuards {
		if sleepMins > mostSleep {
			mostSleep = sleepMins
			sleepiestGuard = guardID
		}
	}
	log.Printf("Guard %d is the sleepiest, with %d minutes asleep", sleepiestGuard, mostSleep)

	var sleepsPerMin [60]int
	for _, day := range days {
		if day.GuardID != sleepiestGuard {
			continue
		}
		for i, asleep := range day.Asleep {
			if asleep {
				sleepsPerMin[i]++
			}
		}
	}
	mostSleep = 0
	var sleepiestMin int
	for min, sleeps := range sleepsPerMin {
		if sleeps > mostSleep {
			mostSleep = sleeps
			sleepiestMin = min
		}
	}
	log.Printf("Guard %d's sleepiest minute is %d", sleepiestGuard, sleepiestMin)
	log.Printf("Puzzle 1 answer is %d", sleepiestGuard*sleepiestMin)

	guardSleepMinutes := make(map[int]*[60]int)
	for _, day := range days {
		for i, asleep := range day.Asleep {
			if asleep {
				m, exists := guardSleepMinutes[day.GuardID]
				if !exists {
					var b [60]int
					guardSleepMinutes[day.GuardID] = &b
					m = &b
				}
				(*m)[i]++
			}
		}
	}
	mostSleep = 0
	sleepiestGuard = 0
	sleepiestMin = 0
	for guardID, m := range guardSleepMinutes {
		for i, sleeps := range *m {
			if sleeps > mostSleep {
				mostSleep = sleeps
				sleepiestGuard = guardID
				sleepiestMin = i
			}
		}
	}
	log.Printf("Guard %d's slept at minute %d on %d days", sleepiestGuard, sleepiestMin, mostSleep)
	log.Printf("Puzzle 2 answer is %d", sleepiestGuard*sleepiestMin)
}

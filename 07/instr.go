package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
)

type Node byte

const Idle = Node(0)

type NodeSet map[Node]struct{}

type NodeList []Node

type EdgeSet map[Node]NodeSet

type Graph struct {
	Nodes      NodeSet
	Required   EdgeSet
	RequiredBy EdgeSet
}

func LoadGraph(r io.Reader) Graph {
	ret := Graph{
		Nodes:      make(NodeSet),
		Required:   make(EdgeSet),
		RequiredBy: make(EdgeSet),
	}
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		l := sc.Text()
		before := Node(l[5])
		after := Node(l[36])
		ret.Nodes.Add(before)
		ret.Nodes.Add(after)
		ret.Required.Add(after, before)
		ret.RequiredBy.Add(before, after)
	}
	return ret
}

func (s Node) String() string {
	if s == Idle {
		return "."
	}
	return string(s)
}

func (s Node) TimePenalty() int {
	return (int(s) - 'A')
}

func (s NodeSet) String() string {
	letters := make([]string, 0, len(s))
	for n := range s {
		letters = append(letters, string(n))
	}
	sort.Strings(letters)
	return fmt.Sprintf("NodeSet(%s)", strings.Join(letters, ""))
}

func (s NodeSet) Has(n Node) bool {
	_, ok := s[n]
	return ok
}

func (s NodeSet) Add(n Node) {
	s[n] = struct{}{}
}

func (l NodeList) Less(i, j int) bool {
	return l[i] < l[j]
}

func (l NodeList) Len() int {
	return len(l)
}

func (l NodeList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (s NodeList) String() string {
	letters := make([]string, 0, len(s))
	for _, n := range s {
		letters = append(letters, string(n))
	}
	return fmt.Sprintf("NodeList(%s)", strings.Join(letters, ""))
}

func (s EdgeSet) Has(after, before Node) bool {
	ns, ok := s[after]
	if !ok {
		return false
	}
	return ns.Has(before)
}

func (s EdgeSet) Add(after, before Node) {
	ns, ok := s[after]
	if !ok {
		ns = make(NodeSet)
		s[after] = ns
	}
	ns.Add(before)
}

func (s EdgeSet) Targets(of Node) NodeSet {
	return s[of]
}

func (g Graph) StartNodes() NodeSet {
	ret := make(NodeSet)
	for n := range g.Nodes {
		if len(g.Required.Targets(n)) == 0 {
			ret.Add(n)
		}
	}
	return ret
}

func (g Graph) NodeOrder() NodeList {
	// Our return value and our queue share the same backing array. We'll
	// gradually produce the return value at the front of this buffer
	// as a side-effect of maintaining the queue.
	ret := make(NodeList, len(g.Nodes))
	queue := ret[:0]

	// This is a variant of Kahn's topological sort algorithm where we retain
	// the indegree count in a separate map rather than destroy the edges in
	// the graph as we go.
	awaiting := make(map[Node]int, len(g.Nodes))
	for node := range g.Nodes {
		prereqs := g.Required.Targets(node)
		awaiting[node] = len(prereqs)
		if len(prereqs) == 0 {
			queue = append(queue, node)
		}
	}
	sort.Sort(queue)

	for len(queue) > 0 {
		var next Node
		log.Printf("queue is now %s", queue)
		next, queue = queue[0], queue[1:]
		log.Printf("took %s with remaining queue %s", next, queue)

		for maybe := range g.RequiredBy.Targets(next) {
			awaiting[maybe]--
			if awaiting[maybe] < 1 {
				queue = append(queue, maybe)
			}
		}
		// The puzzle calls for us to process all ready nodes in alphabetical
		// order, even if they don't arrive in the queue in that order.
		sort.Sort(queue)
	}
	return ret
}

func (g Graph) WorkIt(workerCount int, baseTime int) {
	type Worker struct {
		Current Node
		Remain  int
	}
	workers := make([]Worker, workerCount)

	queue := make(NodeList, 0, len(g.Nodes))
	done := queue
	awaiting := make(map[Node]int, len(g.Nodes))
	for node := range g.Nodes {
		prereqs := g.Required.Targets(node)
		awaiting[node] = len(prereqs)
		if len(prereqs) == 0 {
			queue = append(queue, node)
		}
	}

	t := 0
	for {
		sort.Sort(queue)
		for i := range workers {
			// Update worker status
			if workers[i].Current != Idle {
				if workers[i].Remain == 0 {
					completed := workers[i].Current
					done = append(done, completed)
					for maybe := range g.RequiredBy.Targets(completed) {
						awaiting[maybe]--
						if awaiting[maybe] < 1 {
							queue = append(queue, maybe)
						}
					}
					workers[i].Current = Idle
				} else {
					workers[i].Remain--
				}
			}
			// Assign more work if possible
			if len(queue) > 0 && workers[i].Current == Idle {
				var next Node
				next, queue = queue[0], queue[1:]
				workers[i].Current = next
				workers[i].Remain = baseTime + next.TimePenalty()
			}
		}
		var logBuf bytes.Buffer
		fmt.Fprintf(&logBuf, " %3d", t)
		for _, w := range workers {
			fmt.Fprintf(&logBuf, "  %s", w.Current)
		}
		fmt.Fprintf(&logBuf, "   %s", done)
		log.Print(logBuf.String())
		t++
		if len(queue) == 0 {
			inProgress := 0
			for _, w := range workers {
				if w.Current != Idle {
					inProgress++
				}
			}
			if inProgress == 0 {
				break
			}
		}
	}
}

func main() {
	graph := LoadGraph(os.Stdin)
	log.Printf("Will start with %s", graph.StartNodes().String())
	topoOrder := graph.NodeOrder()
	log.Printf("Traversal order is %s", topoOrder)
	workerCount := 5
	baseTime := 60
	if len(graph.Nodes) < 8 {
		// Test input
		workerCount = 2
		baseTime = 0
	}
	graph.WorkIt(workerCount, baseTime)
}
